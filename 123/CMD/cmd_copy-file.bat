// windows--Takeown、Cacls、Icacls-文件、文件夹夺权用法 /////////////////////////

takeown /?
// 强制将当前目录下的所有文件及文件夹、子文件夹下的所有者更改为管理员组(administrators)
takeown /F "D:\Program Files" /A /R /D Y

// 获取该文件的所属权
takeown   /f 文件名      

// 获取整个文件夹及其下面子目录文件的所属权
takeown /f * /a /r /d y

// 将所有d:\file1目录下的文件、子文件夹的NTFS权限修改为仅管理员组(administrators)完全控制(删除原有NTFS权限设置)
cacls d:\file1\*.* /T /G administrators:F


// 在原有d:\file1目录下的文件、子文件夹的NTFS权限上添加管理员组(administrators)完全控制权限(不删除原有NTFS权限设置)：
cacls d:\file1\*.* /T /E /G administrators:F

// 将 c:\windows 及其子目录下所有文件的ACL 保存到 AclFile。
icacls c:\windows\* /save AclFile /T

// 将还原 c:\windows 及其子目录下存在的 AclFile 内所有文件的 ACL。
icacls c:\windows\ /restore AclFile

// 将授予用户对文件删除和写入 DAC 的管理员权限。
icacls file /grant Administrator:(D,WDAC)

// 将授予由 sid S-1-1-0 定义的用户对文件删除和写入 DAC 的权限。
icacls file /grant *S-1-1-0:(D,WDAC)
///////////////////////////////////////////////////////////////////////////
// 谨慎使用，会删除文件
rd /s /q "D:\Program Files\xx"


@echo off
echo 当前盘符：%~d0
echo 当前盘符和路径：%~dp0
echo 当前批处理全路径：%~f0
echo 当前盘符和路径的短文件名格式：%~sdp0
echo 当前CMD默认目录：%cd%
echo 目录中有空格也可以加入""避免找不到路径
echo 当前盘符："%~d0"
echo 当前盘符和路径："%~dp0"
echo 当前批处理全路径："%~f0"
echo 当前盘符和路径的短文件名格式："%~sdp0"
echo 当前CMD默认目录："%cd%"
pause

::+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

cls
@echo *******************************************************************************************
@echo 复制文件夹及子文件下所有特定文件，并且复制存在特定文件的子文件夹。
@echo *******************************************************************************************
@echo                                          *
@echo *******************************************************************************************
::目的文件夹名为mubiao
::创建自己要保存文件的目录，这里是“F:\repo\YYY\M1pluspatteren2022\11\11”。去掉@
@MD F:\repo\YYY\M1pluspatteren2022\11\11
@set mubiao=F:\repo\YYY\M1pluspatteren2022\11\11
::原文件夹yuanlujing
@set yuanlujing="%~dp0"
@echo  从这里 %mubiao% 
@echo 拷贝到： %yuanlujing%
@echo *******************************************************************************************
@pause

cls
@echo on
@echo 文件内容较多，请稍等片刻会自动开始
::复制特定文件。
cd %yuanlujing%
for /f "delims=" %%a in ('dir/b/s/a-d *.jpg *.jpeg') do (
   :: /f 解析文本内容字符串
   :: delims切分，token提取
   :: 设置变量！
   set "var=%%a"
   :: 本地环境变量延迟扩展，!!号包裹的变量动态变化的。
   setlocal enabledelayedexpansion
   ::set var=!var:*%cd%=!
   ::如果不复制子文件夹，注释后删除掉下行(也就是会创建子目录)
   ::创建子目录
   ::md "%mubiao%!var!" 
   ::复制子目录指定变量类型的文件。
   ::copy /y "%%a" "%mubiao%!var!"
   copy /y "%%a" "%mubiao%"
   endlocal
   ::结束
)
pause

::++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


// windows 启动服务脚本
echo off
:: 开启 VMwareHostd 服务后，VMAuthdService、VMUSBArbService 也会自动启动
net start "VMwareHostd"
net start "VMnetDHCP"
net start "VMware NAT Service"

:: 例如：start "" "D:\Program Files\VMware Workstation\vmware.exe"
start "" "D:\Program Files\VMware Workstation\vmware.exe"
exit