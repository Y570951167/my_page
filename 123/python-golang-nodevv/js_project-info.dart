├── vue.config.js/                      # webpack 配置文件；
├── config/                             # 与项目构建相关的常用的配置选项；
│   ├── index.js                        # 主配置文件
│        
├── src/        
│   ├── main.js                         # webpack 的入口文件；
│   ├── App.vue                         # APP页面入口
│   ├── assets/                         # 共用的代码以外的资源，如：图片、图标、视频 等；
│   ├── api/                            # 网络模块，如：接口；
│   ├── router/                         # 路由模块
│   ├── I18n/                           # 国际化模块
│   ├── directive/                      # 指令
│   ├── vuex/                           # 组件共享状态
│   ├── libs/                           # 工具
│   ├── components/                     # 共用的组件；； 这里的存放的组件应该都是展示组件
│   │   ├── base/                       # 基本组件，如：共用的弹窗组件，loading加载组件，提示组件。              
│   ├── utils/                          # 共用的资源，如：常用的图片、图标，共用的组件、模块、样式，常量文件等等；        
│   │   ├── util/                      # 自己封装的一些工具
│   │   └── ...        
│   ├── layout                          # 公共局页面；
│   └── view/                           # 存放项目业务代码；
│       ├── home                        # 首页；
│       ├── application-service         # 应用服务
│       ├── image                       # 镜像
│       ├── team                        # 团队成员
│       ├── code-manage                 # 代码管理
├── App.vue                             # app 的根组件；
├── public/                             # 纯静态资源，该目录下的文件不会被webpack处理，该目录会被拷贝到输出目录下；        
├── .babelrc                            # babel 的配置文件
├── .editorconfig                       # 编辑器的配置文件；可配置如缩进、空格、制表类似的参数；
├── .eslintrc.js                        # eslint 的配置文件
├── .eslintignore                       # eslint 的忽略规则
├── .gitignore                          # git的忽略配置文件
├── jsconfig.json                       # 别名配置，用于ctrl+鼠标左建查看文件
├── package.json                        # npm包配置文件，里面定义了项目的npm脚本，依赖包等信息
└── README.md                           # 项目信息文档

1、README.md 帮助文档
2、package.json 配置
3、main.js 入口
   1. 全局组件
   2. 全局方法
   3. 全局指令    

1、router 路由
2、store 数据存储
3、components 公共组件
4、utils 常用工具

// 别名配置，
{
  alias: {
    // 设置路径
    '~': path.resolve(__dirname, './'),
    // 设置别名
    '@': path.resolve(__dirname, './src')
  },
  extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json', '.vue']
},

// 跨域：处理后端接口
proxy: {
  '/dev-api': {
    target: 'http://localhost:8080',
    changeOrigin: true,
    rewrite: (p) => p.replace(/^\/dev-api/, '')
  }
}

// 入口文件 main.js
import { createApp } from 'vue'
​
import Cookies from 'js-cookie'
​
import ElementPlus from 'element-plus'
import locale from 'element-plus/lib/locale/lang/zh-cn' // 中文语言
​
import '@/assets/styles/index.scss' // global css
​
import App from './App'
import store from './store'
import router from './router'
import directive from './directive' // directive
​
​
// 注册指令
import plugins from './plugins' // plugins
import { download } from '@/utils/request'
​
// svg图标
import 'virtual:svg-icons-register'
import SvgIcon from '@/components/SvgIcon'
import elementIcons from '@/components/SvgIcon/svgicon'
​
/**
 * permission这个是权限 ， 一搬会做一些请求或打开页面前的操作，如身份验证，用户登录有效期等
 * 
*/
import './permission' // permission control
​
import { useDict } from '@/utils/dict'
import { parseTime, resetForm, addDateRange, handleTree, selectDictLabel } from '@/utils/ruoyi'
​
// 分页组件
import Pagination from '@/components/Pagination'
// 自定义表格工具组件
import RightToolbar from '@/components/RightToolbar'
// 文件上传组件
import FileUpload from "@/components/FileUpload"
// 图片上传组件
import ImageUpload from "@/components/ImageUpload"
// 图片预览组件
import ImagePreview from "@/components/ImagePreview"
// 自定义树选择组件
import TreeSelect from '@/components/TreeSelect'
// 字典标签组件
import DictTag from '@/components/DictTag'
​
const app = createApp(App)
​
// 全局方法挂载
app.config.globalProperties.useDict = useDict
app.config.globalProperties.download = download
app.config.globalProperties.parseTime = parseTime
app.config.globalProperties.resetForm = resetForm
app.config.globalProperties.handleTree = handleTree
app.config.globalProperties.addDateRange = addDateRange
app.config.globalProperties.selectDictLabel = selectDictLabel
​
// 全局组件挂载
app.component('DictTag', DictTag)
app.component('Pagination', Pagination)
app.component('TreeSelect', TreeSelect)
app.component('FileUpload', FileUpload)
app.component('ImageUpload', ImageUpload)
app.component('ImagePreview', ImagePreview)
app.component('RightToolbar', RightToolbar)
​
app.use(router)
app.use(store)
app.use(plugins)
app.use(elementIcons)
app.component('svg-icon', SvgIcon)
​
directive(app)
​
// 使用element-plus 并且设置全局的大小
app.use(ElementPlus, {
  locale: locale,
  // 支持 large、default、small
  size: Cookies.get('size') || 'default'
})
​
app.mount('#app')

// 入口文件 main.js

// 权限、路由拦截：一般就是去登录页，或去其它的页面，获取用信息等常用操作。
NProgress.configure({ showSpinner: false })
// 以下是白名单不需要操作
const whiteList = ['/login', '/auth-redirect', '/bind', '/register']
​
router.beforeEach((to, from, next) => {
  NProgress.start()
  if (getToken()) {
    to.meta.title && store.dispatch('settings/setTitle', to.meta.title)
    /* has token*/
    if (to.path === '/login') {
      next({ path: '/' })
      NProgress.done()
    } else {
      if (store.getters.roles.length === 0) {
        // 判断当前用户是否已拉取完user_info信息
        store.dispatch('GetInfo').then(() => {
          store.dispatch('GenerateRoutes').then(accessRoutes => {
            // 根据roles权限生成可访问的路由表
            router.addRoutes(accessRoutes) // 动态添加可访问路由表
            next({ ...to, replace: true }) // hack方法 确保addRoutes已完成
          })
        }).catch(err => {
            store.dispatch('LogOut').then(() => {
              Message.error(err)
              next({ path: '/' })
            })
          })
      } else {
        next()
      }
    }
  } else {
    // 没有token
    if (whiteList.indexOf(to.path) !== -1) {
      // 在免登录白名单，直接进入
      next()
    } else {
      next(`/login?redirect=${to.fullPath}`) // 否则全部重定向到登录页
      NProgress.done()
    }
  }
})
​
router.afterEach(() => {
  NProgress.done()
})

// 权限、路由拦截


// 统一接口请求
一般做一些常用的请求，报错，权限认证，取消请求等：

配置
    超时
    请求地址
    头部信息


请求前处理
    Authorization
    重复提交
    post、get特殊处理
    失败或异常


响应拦截器
    状态码
    错误信息
    二进制数据则直接返回
    401
    500
    非200
    错误或异常


全局指令
    hasRole按钮权限等
    hasPermission角色权限


全局方法
    下载
    时间
    日期等


全局组件
    分页
    上传
    图片预览
    标签等


公共组件
常用的一些公共组件，这些组件常用，或某一些页面中要用到；如下：
    表单
    table
    动画
    时间
    图片裁剪
    编辑器
    弹窗
    抽屉
    详情
    验证
    二维码
    上传
    加载
    图标
    打印
    水印
    日志

常用工具
常用的一些功能如下：
    右键
    缓存
    权限密码等生成
    字典数据
    常用报错或提示
    表格时间格式化
    克隆
    合并
    截流
    防抖
    首字母大小
    验证等


路由配置
    404报错
    401没有权限
    最外层封装
    左侧栏目
    导航栏目
    标签