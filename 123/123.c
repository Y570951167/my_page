C:\Users\FEI>wmic // powershell所取代
wmic:root\cli>cpu list full


AddressWidth=64
Architecture=9
Availability=3
Caption=Intel64 Family 6 Model 165 Stepping 5
ConfigManagerErrorCode=
ConfigManagerUserConfig=
CpuStatus=1
CreationClassName=Win32_Processor
CurrentClockSpeed=3696
CurrentVoltage=10
DataWidth=64
Description=Intel64 Family 6 Model 165 Stepping 5
DeviceID=CPU0
ErrorCleared=
ErrorDescription=
ExtClock=100
Family=207
InstallDate=
L2CacheSize=2560
L2CacheSpeed=
LastErrorCode=
Level=6
LoadPercentage=4
Manufacturer=GenuineIntel
MaxClockSpeed=3696
Name=Intel(R) Core(TM) i9-10900K CPU @ 3.70GHz
OtherFamilyDescription=
PNPDeviceID=
PowerManagementCapabilities=
PowerManagementSupported=FALSE
ProcessorId=BFEBFBFF000A0655
ProcessorType=3
Revision=
Role=CPU
SocketDesignation=U3E1
Status=OK
StatusInfo=3
Stepping=
SystemCreationClassName=Win32_ComputerSystem
SystemName=DESKTOP-MF9A4OR
UniqueId=
UpgradeMethod=1
Version=
VoltageCaps=

/*
// Edge浏览器意外退出，找的解决办法
    // 扫描受保护的系统文件完整性命令(联网使用)
    sfc /SCANNOW

    // 部署映像服务和管理工具 dism
    // 检测组件ScanHealth
    dism /Online /Cleanup-Image /ScanHealth
    // 检测组件CheckHealth
    dism /Online /Cleanup-Image /CheckHealth
    // 还原组件RestoreHealth
    dism /Online /Cleanup-image /RestoreHealth

*/

// 获取电脑系统信息的脚本
#####################################################################
# システム情報
#####################################################################
function QueryServerInfo(){
    $ReturnData = New-Object PSObject | Select-Object HostName,Manufacturer,Model,SN,CPUName,PhysicalCores,Sockets,MemorySize,DiskInfos,OS

    $Win32_BIOS = Get-WmiObject Win32_BIOS
    $Win32_Processor = Get-WmiObject Win32_Processor
    $Win32_ComputerSystem = Get-WmiObject Win32_ComputerSystem
    $Win32_OperatingSystem = Get-WmiObject Win32_OperatingSystem

    # ホスト名
    $ReturnData.HostName = hostname

    # メーカー名
    $ReturnData.Manufacturer = $Win32_BIOS.Manufacturer

    # モデル名
    $ReturnData.Model = $Win32_ComputerSystem.Model

    # シリアル番号
    $ReturnData.SN = $Win32_BIOS.SerialNumber

    # CPU 名
    $ReturnData.CPUName = @($Win32_Processor.Name)[0]

    # 物理コア数
    $PhysicalCores = 0
    $Win32_Processor.NumberOfCores | % { $PhysicalCores += $_}
    $ReturnData.PhysicalCores = $PhysicalCores
    
    # ソケット数
    $ReturnData.Sockets = $Win32_ComputerSystem.NumberOfProcessors
    
    # メモリーサイズ(GB)
    $Total = 0
    Get-WmiObject -Class Win32_PhysicalMemory | % {$Total += $_.Capacity}
    $ReturnData.MemorySize = [int]($Total/1GB)
    
    # ディスク情報
    [array]$DiskDrives = Get-WmiObject Win32_DiskDrive | ? {$_.Caption -notmatch "Msft"} | sort Index
    $DiskInfos = @()
    foreach( $DiskDrive in $DiskDrives ){
        $DiskInfo = New-Object PSObject | Select-Object Index, DiskSize
        $DiskInfo.Index = $DiskDrive.Index              # ディスク番号
        $DiskInfo.DiskSize = [int]($DiskDrive.Size/1GB) # ディスクサイズ(GB)
        $DiskInfos += $DiskInfo
    }
    $ReturnData.DiskInfos = $DiskInfos
    
    # OS 
    $OS = $Win32_OperatingSystem.Caption
    $SP = $Win32_OperatingSystem.ServicePackMajorVersion
    if( $SP -ne 0 ){ $OS += "SP" + $SP }
    $ReturnData.OS = $OS
    
    return $ReturnData
}

// 