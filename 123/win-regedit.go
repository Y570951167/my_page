// AcceptLanguage--zh-HK,zh;q=0.7,zh-CN;q=0.3
// WINXP:
// MRX3F-47B9T-2487J-KWKMF-RPWBY

// Office 2024激活（Windows PowerShell）
// Microsoft Activation Scripts 是一个开源脚本，它是一款用于激活 Windows 和 Office 产品的工具。
irm https://get.activated.win | iex

// Windows激活检查
slmgr /xpr


// Office更改默认安装目录
// 新建安装目录
D:\Program Files\Microsoft Office

mklink /j "C:\Program Files\Microsoft Office" "D:\Program Files\Microsoft Office"


移动宽带
user--f4jk7sks