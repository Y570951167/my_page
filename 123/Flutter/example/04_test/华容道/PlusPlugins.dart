///
/// 额外添加的class系列
/// 

///定义系列的返回特定类型的函数,只有一个参数
typedef RBoolOneP<T> = bool Function(T obj);
typedef RVoidOneP<T> = void Function(T obj);
typedef RIntOneP<T> = int Function(T obj);

typedef BBB<T> = int Function(List<T> obj);

///定义系列的返回特定类型的函数,无参数
typedef RBoolZeroP = bool Function();
typedef RVoidZeroP = void Function();
typedef RIntZeroP = int Function();