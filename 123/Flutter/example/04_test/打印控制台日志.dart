import 'package:flutter/material.dart';

// main方法，箭头函数，runApp()调用MyApp()类
void main() => runApp(const MyApp());

// 创建root视图类 MyApp 继承自 StatelessWidget
class MyApp extends StatelessWidget {
  const MyApp({super.key});

  void showConsoleUsingPrint() {
    print('Console Message Using Print');
  }

  void showConsoleUsingDebugPrint() {
    debugPrint('Console Message Using Debug Print');
  }

  @override
  Widget build(BuildContext context) {
    print('Widget Build Start');

    // MyApp类的小部件构建区域
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        home: Scaffold(
            body: Center(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
              // 第一个凸起按钮
              Container(
                  margin: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child: ElevatedButton(
                    // onPress事件调用
                    onPressed: () => showConsoleUsingPrint(),
                    style: const ButtonStyle(
                      textStyle: MaterialStatePropertyAll(
                          TextStyle(color: Colors.white)),
                      backgroundColor: MaterialStatePropertyAll(Colors.green),
                      padding: MaterialStatePropertyAll(
                          EdgeInsets.fromLTRB(12, 12, 12, 12)),
                    ),
                    child: const Text(' Print Console Message using Print '),
                  )),
              // 第二个凸起按钮
              Container(
                  margin: const EdgeInsets.fromLTRB(10, 10, 10, 10),
                  child: ElevatedButton(
                    // onPress事件调用
                    onPressed: () => showConsoleUsingDebugPrint(),
                    style: const ButtonStyle(
                      textStyle: MaterialStatePropertyAll(
                          TextStyle(color: Colors.white)),
                      backgroundColor: MaterialStatePropertyAll(Colors.green),
                      padding: MaterialStatePropertyAll(
                          EdgeInsets.fromLTRB(12, 12, 12, 12)),
                    ),
                    child:
                        const Text(' Print Console Message using Debug Print '),
                  )),
            ]))));
  }
}
