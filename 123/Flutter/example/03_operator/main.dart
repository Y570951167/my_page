import 'package:flutter/material.dart';

void main() => runApp(const MyApp());

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  final int a = 20;

  final int b = 30;

  final bool c = true;

  void checkWithCondition() {
    (a > b) ? print('A is Big') : print('B is Big');
  }

  void checkWithBoolean() {
    c ? print('C is True') : print('C is False');
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            body: Center(
                child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
          Container(
              margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: ElevatedButton(
                onPressed: () => checkWithCondition(),
                style: const ButtonStyle(
                    textStyle: MaterialStatePropertyAll(
                        TextStyle(backgroundColor: Colors.white)),
                    foregroundColor: MaterialStatePropertyAll(Colors.green),
                    padding: MaterialStatePropertyAll(
                        EdgeInsets.fromLTRB(12, 12, 12, 12))),
                child: const Text('Call Ternary Operator With Condition'),
              )),
          Container(
              margin: const EdgeInsets.fromLTRB(20, 0, 20, 0),
              child: ElevatedButton(
                onPressed: () => checkWithBoolean(),
                style: const ButtonStyle(
                    textStyle: MaterialStatePropertyAll(
                        TextStyle(backgroundColor: Colors.white)),
                    foregroundColor: MaterialStatePropertyAll(Colors.green),
                    padding: MaterialStatePropertyAll(
                        EdgeInsets.fromLTRB(12, 12, 12, 12))),
                child: const Text('Call Ternary Operator With Boolean Value'),
              )),
        ]))));
  }
}
