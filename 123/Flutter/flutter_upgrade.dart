1.git checkout -- . //注意字符不要错,flutter安装目录
2.flutter upgrade --force
-----------------Flutter升级--------------------
D:\flutter_windows_3.3.9-stable\flutter>git checkout -- .

D:\flutter_windows_3.3.9-stable\flutter>flutter upgrade
Your flutter checkout has local changes that would be erased by upgrading. If you want to keep these changes, it is
recommended that you stash them via "git stash" or else commit the changes to a local branch. If it is okay to remove
local changes, then re-run this command with "--force".

D:\flutter_windows_3.3.9-stable\flutter>flutter upgrade --force
Upgrading Flutter to 3.13.0-19.0.pre.78 from 3.11.0-6.0.pre.85 in D:\flutter_windows_3.3.9-stable\flutter...
Checking Dart SDK version...
Downloading Dart SDK from Flutter engine 146c4c9487fc68ff97dbf900bf3c760c60ed251a...
Expanding downloaded archive...
Building flutter tool...
Running pub upgrade...
Resolving dependencies...
Got dependencies.

Upgrading engine...
Flutter assets will be downloaded from https://storage.flutter-io.cn. Make sure you trust this source!
Downloading android-arm-profile/windows-x64 tools...             2,322ms
Downloading android-arm-release/windows-x64 tools...             1,783ms
Downloading android-arm64-profile/windows-x64 tools...           2,089ms
Downloading android-arm64-release/windows-x64 tools...           1,872ms
Downloading android-x64-profile/windows-x64 tools...             2,022ms
Downloading android-x64-release/windows-x64 tools...             1,837ms
Downloading android-x86 tools...                                   12.8s
Downloading android-x64 tools...                                   12.2s
Downloading android-arm tools...                                   11.6s
Downloading android-arm-profile tools...                            6.0s
Downloading android-arm-release tools...                            4.1s
Downloading android-arm64 tools...                                 11.8s
Downloading android-arm64-profile tools...                          6.5s
Downloading android-arm64-release tools...                          4.7s
Downloading android-x64-profile tools...                            6.7s
Downloading android-x64-release tools...                            4.8s
Downloading android-x86-jit-release tools...                        8.3s
Downloading Web SDK...                                             43.9s
Downloading package sky_engine...                                1,078ms
Downloading flutter_patched_sdk tools...                            3.1s
Downloading flutter_patched_sdk_product tools...                    3.0s
Downloading windows-x64 tools...                                   33.5s
Downloading windows-x64-debug/windows-x64-flutter tools...         65.8s
Downloading windows-x64/flutter-cpp-client-wrapper tools...        151ms
Downloading windows-x64-profile/windows-x64-flutter tools...        59.5s
Downloading windows-x64-release/windows-x64-flutter tools...        53.9s
Downloading windows-x64/font-subset tools...                     1,357ms

Flutter 3.13.0-19.0.pre.78 • channel master • https://github.com/flutter/flutter.git
Framework • revision 436df69a46 (10 hours ago) • 2023-08-08 01:58:16 -0400
Engine • revision 146c4c9487
Tools • Dart 3.2.0 (build 3.2.0-42.0.dev) • DevTools 2.26.1

Running flutter doctor...
Flutter assets will be downloaded from https://storage.flutter-io.cn. Make sure you trust this source!
Doctor summary (to see all details, run flutter doctor -v):
[√] Flutter (Channel master, 3.13.0-19.0.pre.78, on Microsoft Windows [版本 10.0.19045.3208], locale zh-CN)
[√] Windows Version (Installed version of Windows is version 10 or higher)
[√] Android toolchain - develop for Android devices (Android SDK version 33.0.0-rc3)
[√] Chrome - develop for the web
[√] Visual Studio - develop Windows apps (Visual Studio Community 2022 17.6.0)
[√] Android Studio (version 2022.3)
[√] Connected device (3 available)
[!] Network resources
    X A network error occurred while checking "https://github.com/": 信号灯超时时间已到


! Doctor found issues in 1 category.

This channel is intended for Flutter contributors. This channel is not as thoroughly tested as the "beta" and "stable" channels. We do not recommend using this channel for normal use as it more likely to contain serious regressions.

For information on contributing to Flutter, see our contributing guide:
    https://github.com/flutter/flutter/blob/master/CONTRIBUTING.md

For the most up to date stable version of flutter, consider using the "beta" channel instead. The Flutter "beta" channel enjoys all the same automated testing as the "stable" channel, but is updated roughly once a month instead of once a quarter.
To change channel, run the "flutter channel beta" command.
-----------------Flutter升级--------------------